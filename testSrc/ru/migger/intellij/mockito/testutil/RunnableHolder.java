package ru.migger.intellij.mockito.testutil;

import com.intellij.openapi.application.Application;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;

public class RunnableHolder {
    private Runnable runnable;

    public Runnable getRunnable() {
        return runnable;
    }

    public void setRunnable(Runnable runnable) {
        this.runnable = runnable;
    }

    public static RunnableHolder getApplicationWriteActionHolder(Application application) {
        final RunnableHolder runnableHolder = new RunnableHolder();
        doAnswer(invocation -> {
            runnableHolder.setRunnable((Runnable) invocation.getArguments()[0]);
            return null;
        }).when(application).runWriteAction(any(Runnable.class));
        return runnableHolder;
    }
}
