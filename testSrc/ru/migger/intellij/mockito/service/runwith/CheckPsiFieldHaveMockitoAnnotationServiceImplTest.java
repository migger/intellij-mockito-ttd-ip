package ru.migger.intellij.mockito.service.runwith;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.psi.PsiField;
import org.junit.Before;
import org.junit.Test;
import ru.migger.intellij.mockito.service.common.field.CheckFieldAnnotationService;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CheckPsiFieldHaveMockitoAnnotationServiceImplTest {
    private CheckPsiFieldHaveMockitoAnnotationService checkPsiFieldHaveMockitoAnnotationService;
    private CheckFieldAnnotationService checkFieldAnnotationService;
    @Before
    public void setUp() throws Exception {
        checkFieldAnnotationService = mock(CheckFieldAnnotationService.class);
        checkPsiFieldHaveMockitoAnnotationService = new CheckPsiFieldHaveMockitoAnnotationServiceImpl(checkFieldAnnotationService);
    }
    @Test
    public void testIsApplicationComponent() throws Exception {
        assertTrue(checkPsiFieldHaveMockitoAnnotationService instanceof ApplicationComponent);
    }

    @Test
    public void testBoth() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        when(checkFieldAnnotationService.isFieldAnnotated(psiField, "org.mockito.Mock")).thenReturn(true);
        when(checkFieldAnnotationService.isFieldAnnotated(psiField, "org.mockito.InjectMocks")).thenReturn(true);
        assertTrue(checkPsiFieldHaveMockitoAnnotationService.isPsiFiledHaveMockitoAnnotation(psiField));
    }

    @Test
    public void testFirst() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        when(checkFieldAnnotationService.isFieldAnnotated(psiField, "org.mockito.Mock")).thenReturn(false);
        when(checkFieldAnnotationService.isFieldAnnotated(psiField, "org.mockito.InjectMocks")).thenReturn(true);
        assertTrue(checkPsiFieldHaveMockitoAnnotationService.isPsiFiledHaveMockitoAnnotation(psiField));
    }

    @Test
    public void testSecond() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        when(checkFieldAnnotationService.isFieldAnnotated(psiField, "org.mockito.Mock")).thenReturn(true);
        when(checkFieldAnnotationService.isFieldAnnotated(psiField, "org.mockito.InjectMocks")).thenReturn(false);
        assertTrue(checkPsiFieldHaveMockitoAnnotationService.isPsiFiledHaveMockitoAnnotation(psiField));
    }

    @Test
    public void testNone() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        when(checkFieldAnnotationService.isFieldAnnotated(psiField, "org.mockito.Mock")).thenReturn(false);
        when(checkFieldAnnotationService.isFieldAnnotated(psiField, "org.mockito.InjectMocks")).thenReturn(false);
        assertFalse(checkPsiFieldHaveMockitoAnnotationService.isPsiFiledHaveMockitoAnnotation(psiField));
    }

    //    @Test
//    public void testGetModifierListReturnsNull() throws Exception {
//        final PsiField psiField = mock(PsiField.class);
//        assertFalse(checkPsiFieldHaveMockitoAnnotationService.isPsiFiledHaveMockitoAnnotation(psiField));
//    }
//
//    @Test
//    public void testNoAnnotations() throws Exception {
//        final PsiField psiField = mock(PsiField.class);
//        final PsiModifierList psiModifierList = mock(PsiModifierList.class);
//        when(psiField.getModifierList()).thenReturn(psiModifierList);
//        when(psiModifierList.getAnnotations()).thenReturn(new PsiAnnotation[0]);
//        assertFalse(checkPsiFieldHaveMockitoAnnotationService.isPsiFiledHaveMockitoAnnotation(psiField));
//    }
//
//    @Test
//    public void testOtherAnnotation() throws Exception {
//        final PsiField psiField = mock(PsiField.class);
//        final PsiModifierList psiModifierList = mock(PsiModifierList.class);
//        when(psiField.getModifierList()).thenReturn(psiModifierList);
//        final PsiAnnotation psiAnnotation = mock(PsiAnnotation.class);
//        when(psiModifierList.getAnnotations()).thenReturn(new PsiAnnotation[] {
//                psiAnnotation
//        });
//        when(psiAnnotation.getQualifiedName()).thenReturn("org.jetbrains.annotations.NotNull");
//        assertFalse(checkPsiFieldHaveMockitoAnnotationService.isPsiFiledHaveMockitoAnnotation(psiField));
//    }
//
//    @Test
//    public void testMockAnnotation() throws Exception {
//        final PsiField psiField = mock(PsiField.class);
//        final PsiModifierList psiModifierList = mock(PsiModifierList.class);
//        when(psiField.getModifierList()).thenReturn(psiModifierList);
//        final PsiAnnotation psiAnnotation = mock(PsiAnnotation.class);
//        when(psiModifierList.getAnnotations()).thenReturn(new PsiAnnotation[] {
//                psiAnnotation
//        });
//        when(psiAnnotation.getQualifiedName()).thenReturn("org.mockito.Mock");
//        assertTrue(checkPsiFieldHaveMockitoAnnotationService.isPsiFiledHaveMockitoAnnotation(psiField));
//    }
//    @Test
//    public void testInjectMocksAnnotation() throws Exception {
//        final PsiField psiField = mock(PsiField.class);
//        final PsiModifierList psiModifierList = mock(PsiModifierList.class);
//        when(psiField.getModifierList()).thenReturn(psiModifierList);
//        final PsiAnnotation psiAnnotation = mock(PsiAnnotation.class);
//        when(psiModifierList.getAnnotations()).thenReturn(new PsiAnnotation[] {
//                psiAnnotation
//        });
//        when(psiAnnotation.getQualifiedName()).thenReturn("org.mockito.InjectMocks");
//        assertTrue(checkPsiFieldHaveMockitoAnnotationService.isPsiFiledHaveMockitoAnnotation(psiField));
//    }
}
