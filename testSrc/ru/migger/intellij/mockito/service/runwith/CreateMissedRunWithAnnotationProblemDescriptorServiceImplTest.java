package ru.migger.intellij.mockito.service.runwith;

import com.intellij.codeInspection.InspectionManager;
import com.intellij.codeInspection.LocalQuickFix;
import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.util.Pair;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiField;
import org.junit.Before;
import org.junit.Test;
import ru.migger.intellij.mockito.service.common.field.FindPsiFieldWithoutAnnotationBoundsService;
import ru.migger.intellij.mockito.service.common.field.GetPsiFieldParentClassService;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CreateMissedRunWithAnnotationProblemDescriptorServiceImplTest {
    private CreateMissedRunWithAnnotationProblemDescriptorService createMissedRunWithAnnotationProblemDescriptorService;
    private CreateMissedRunWithAnnotationLocalQuickFixService createMissedRunWithAnnotationLocalQuickFixService;
    private GetPsiFieldParentClassService getPsiFieldParentClassService;
    private FindPsiFieldWithoutAnnotationBoundsService findPsiFieldWithoutAnnotationBoundsService;
    private ProblemDescriptor problemDescriptor;
    private InspectionManager inspectionManager;
    private PsiClass psiClass;
    private PsiField psiField;
    private Pair<PsiElement, PsiElement> elementBound;
    private PsiElement fieldStart;
    private PsiElement fieldEnd;

    @Before
    public void setUp() throws Exception {
        psiClass = mock(PsiClass.class);
        psiField = mock(PsiField.class);
        inspectionManager = mock(InspectionManager.class);
        problemDescriptor = mock(ProblemDescriptor.class);

        elementBound = Pair.create(fieldStart = mock(PsiElement.class), fieldEnd = mock(PsiElement.class));

        getPsiFieldParentClassService = mock(GetPsiFieldParentClassService.class);
        findPsiFieldWithoutAnnotationBoundsService = mock(FindPsiFieldWithoutAnnotationBoundsService.class);
        createMissedRunWithAnnotationLocalQuickFixService = mock(CreateMissedRunWithAnnotationLocalQuickFixService.class);
        createMissedRunWithAnnotationProblemDescriptorService = new CreateMissedRunWithAnnotationProblemDescriptorServiceImpl(
                getPsiFieldParentClassService,
                createMissedRunWithAnnotationLocalQuickFixService,
                findPsiFieldWithoutAnnotationBoundsService);

        when(findPsiFieldWithoutAnnotationBoundsService.findPsiFieldWithoutAnnotationsBounds(psiField)).thenReturn(elementBound);
    }

    @Test
    public void testIsApplicationComponent() throws Exception {
        assertTrue(createMissedRunWithAnnotationProblemDescriptorService instanceof ApplicationComponent);
    }

    @Test
    public void testValidDescriptor() throws Exception {
        final LocalQuickFix[] localQuickFixes = new LocalQuickFix[] {mock(LocalQuickFix.class)};
        when(getPsiFieldParentClassService.getParentClass(psiField)).thenReturn(psiClass);
        when(createMissedRunWithAnnotationLocalQuickFixService.createLocalQuickFixes(psiClass)).thenReturn(localQuickFixes);
        when(inspectionManager.createProblemDescriptor(
                fieldStart, fieldEnd,
                "Class is not annotated with @RunWith(MockitoJUnitRunner.class)",
                ProblemHighlightType.GENERIC_ERROR,
                true,
                localQuickFixes)).thenReturn(problemDescriptor);
        assertEquals(problemDescriptor, createMissedRunWithAnnotationProblemDescriptorService.createProblemDescriptor(inspectionManager, psiField));
    }
}