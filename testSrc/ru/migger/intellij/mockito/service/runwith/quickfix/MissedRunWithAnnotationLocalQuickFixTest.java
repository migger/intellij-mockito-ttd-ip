package ru.migger.intellij.mockito.service.runwith.quickfix;

import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.openapi.application.Application;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElementFactory;
import com.intellij.psi.PsiJavaFile;
import com.intellij.psi.PsiModifierList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import ru.migger.intellij.mockito.ClassNames;
import ru.migger.intellij.mockito.service.common.aclass.FindPsiClassService;
import ru.migger.intellij.mockito.service.common.modify.EnsureClassesIsImportedService;
import ru.migger.intellij.mockito.testutil.RunnableHolder;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({PsiElementFactory.SERVICE.class, ApplicationManager.class})
public class MissedRunWithAnnotationLocalQuickFixTest {
    private MissedRunWithAnnotationLocalQuickFix missedXRunWithAnnotationLocalQuickFix;
    private EnsureClassesIsImportedService ensureClassesIsImportedService;
    private FindPsiClassService findPsiClassService;
    private PsiClass psiClass;
    private Application application;

    @Before
    public void setUp() throws Exception {
        application = mock(Application.class);
        mockStatic(PsiElementFactory.SERVICE.class, ApplicationManager.class);
        when(ApplicationManager.getApplication()).thenReturn(application);

        ensureClassesIsImportedService = mock(EnsureClassesIsImportedService.class);
        findPsiClassService = mock(FindPsiClassService.class);
        psiClass = mock(PsiClass.class);
        missedXRunWithAnnotationLocalQuickFix = MissedRunWithAnnotationLocalQuickFix.create(
                findPsiClassService,
                ensureClassesIsImportedService,
                psiClass);
    }

    @Test
    public void testIsLocalQuickFix() throws Exception {
        assertEquals("Add @RunWith(MockitoJUnitRunner.class)", missedXRunWithAnnotationLocalQuickFix.getName());
        assertEquals("MockitoTTD", missedXRunWithAnnotationLocalQuickFix.getFamilyName());
    }

    @Test
    public void testDoFix() throws Exception {
        final Project project = mock(Project.class);
        final ProblemDescriptor problemDescriptor = mock(ProblemDescriptor.class);
        final PsiClass runWith = mock(PsiClass.class);
        final PsiClass mockitoTestRunner = mock(PsiClass.class);
        final PsiModifierList psiModifierList = mock(PsiModifierList.class);
        final PsiJavaFile psiJavaFile = mock(PsiJavaFile.class);
        when(findPsiClassService.findClassByFullName(project, ClassNames.RUN_WITH_ANNOTATION)).thenReturn(runWith);
        when(findPsiClassService.findClassByFullName(project, ClassNames.MOCKITO_JUNIT_RUNNER)).thenReturn(mockitoTestRunner);
        when(runWith.getName()).thenReturn("RunWith");
        when(mockitoTestRunner.getName()).thenReturn("MockitoJUnitRunner");
        when(psiClass.getModifierList()).thenReturn(psiModifierList);
        when(psiClass.getContainingFile()).thenReturn(psiJavaFile);
        final RunnableHolder runnableHolder = RunnableHolder.getApplicationWriteActionHolder(application);
        missedXRunWithAnnotationLocalQuickFix.applyFix(project, problemDescriptor);
        final InOrder order = inOrder(ensureClassesIsImportedService, psiModifierList);
        assertNotNull(runnableHolder.getRunnable());
        order.verify(ensureClassesIsImportedService).ensureIsImported(psiJavaFile, runWith);
        order.verify(ensureClassesIsImportedService).ensureIsImported(psiJavaFile, mockitoTestRunner);
        order.verify(psiModifierList, never()).addAnnotation("RunWith(MockitoJUnitRunner.class)");
        runnableHolder.getRunnable().run();
        order.verify(psiModifierList).addAnnotation("RunWith(MockitoJUnitRunner.class)");
    }

    @Test
    public void testNoRunWithClass() throws Exception {
        final Project project = mock(Project.class);
        final ProblemDescriptor problemDescriptor = mock(ProblemDescriptor.class);
        final PsiClass runWith = mock(PsiClass.class);
        final PsiClass mockitoTestRunner = mock(PsiClass.class);
        final PsiModifierList psiModifierList = mock(PsiModifierList.class);
        final PsiJavaFile psiJavaFile = mock(PsiJavaFile.class);
        when(findPsiClassService.findClassByFullName(project, ClassNames.RUN_WITH_ANNOTATION)).thenReturn(null);
        when(findPsiClassService.findClassByFullName(project, ClassNames.MOCKITO_JUNIT_RUNNER)).thenReturn(mockitoTestRunner);
        when(runWith.getName()).thenReturn("RunWith");
        when(mockitoTestRunner.getName()).thenReturn("MockitoJUnitRunner");
        when(psiClass.getModifierList()).thenReturn(psiModifierList);
        when(psiClass.getContainingFile()).thenReturn(psiJavaFile);
        final RunnableHolder runnableHolder = RunnableHolder.getApplicationWriteActionHolder(application);
        missedXRunWithAnnotationLocalQuickFix.applyFix(project, problemDescriptor);
        assertNull(runnableHolder.getRunnable());
    }

    @Test
    public void testNoJUnitRunner() throws Exception {
        final Project project = mock(Project.class);
        final ProblemDescriptor problemDescriptor = mock(ProblemDescriptor.class);
        final PsiClass runWith = mock(PsiClass.class);
        final PsiClass mockitoTestRunner = mock(PsiClass.class);
        final PsiModifierList psiModifierList = mock(PsiModifierList.class);
        final PsiJavaFile psiJavaFile = mock(PsiJavaFile.class);
        when(findPsiClassService.findClassByFullName(project, ClassNames.RUN_WITH_ANNOTATION)).thenReturn(runWith);
        when(findPsiClassService.findClassByFullName(project, ClassNames.MOCKITO_JUNIT_RUNNER)).thenReturn(null);
        when(runWith.getName()).thenReturn("RunWith");
        when(mockitoTestRunner.getName()).thenReturn("MockitoJUnitRunner");
        when(psiClass.getModifierList()).thenReturn(psiModifierList);
        when(psiClass.getContainingFile()).thenReturn(psiJavaFile);
        final RunnableHolder runnableHolder = RunnableHolder.getApplicationWriteActionHolder(application);
        missedXRunWithAnnotationLocalQuickFix.applyFix(project, problemDescriptor);
        assertNull(runnableHolder.getRunnable());
    }

    @Test
    public void testDoFixModifyerListIsNull() throws Exception {
        final Project project = mock(Project.class);
        final ProblemDescriptor problemDescriptor = mock(ProblemDescriptor.class);
        missedXRunWithAnnotationLocalQuickFix.applyFix(project, problemDescriptor);
        verify(ensureClassesIsImportedService, never()).ensureIsImported(any(), any());
    }
}