package ru.migger.intellij.mockito.service.common.field;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.psi.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FindPsiFieldInitializerClassServiceImplTest {
    private FindPsiFieldInitializerClassService findPsiFieldInitializerClassService;

    @Before
    public void setUp() throws Exception {
        findPsiFieldInitializerClassService = new FindPsiFieldInitializerClassServiceImpl();
    }

    @Test
    public void testIsApplicationComponent() throws Exception {
        assertTrue(findPsiFieldInitializerClassService instanceof ApplicationComponent);
    }

    @Test
    public void testWorks() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final PsiExpression psiExpression = mock(PsiExpression.class);
        final PsiClassType psiClassType = mock(PsiClassType.class);
        final PsiClass expected = mock(PsiClass.class);
        when(psiClassType.resolve()).thenReturn(expected);
        when(psiExpression.getType()).thenReturn(psiClassType);
        when(psiField.getInitializer()).thenReturn(psiExpression);
        assertEquals(expected, findPsiFieldInitializerClassService.findInitializerClass(psiField));
    }

    @Test
    public void testInitializerIsNull() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        assertNull(findPsiFieldInitializerClassService.findInitializerClass(psiField));
    }

    @Test
    public void testInitializerGetTypeIsNull() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        when(psiField.getInitializer()).thenReturn(mock(PsiExpression.class));
        assertNull(findPsiFieldInitializerClassService.findInitializerClass(psiField));
    }

    @Test
    public void testInitializerGetTypeIsNotClass() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final PsiExpression psiExpression = mock(PsiExpression.class);
        when(psiField.getInitializer()).thenReturn(psiExpression);
        when(psiExpression.getType()).thenReturn(mock(PsiType.class));
        assertNull(findPsiFieldInitializerClassService.findInitializerClass(psiField));
    }
}