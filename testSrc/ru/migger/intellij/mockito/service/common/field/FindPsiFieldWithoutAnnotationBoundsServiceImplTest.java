package ru.migger.intellij.mockito.service.common.field;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.util.Pair;
import com.intellij.psi.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FindPsiFieldWithoutAnnotationBoundsServiceImplTest {
    private FindPsiFieldWithoutAnnotationBoundsService findPsiFieldWithoutAnnotationBoundsService;
    private PsiField psiField;

    @Before
    public void setUp() throws Exception {
        psiField = mock(PsiField.class);

        findPsiFieldWithoutAnnotationBoundsService = new FindPsiFieldWithoutAnnotationBoundsServiceImpl();

    }

    @Test
    public void isApplicationCompoment() throws Exception {
        assertTrue(findPsiFieldWithoutAnnotationBoundsService instanceof ApplicationComponent);
    }

    @Test
    public void testNoModifyerList() throws Exception {
        final PsiElement startElement = mock(PsiElement.class);
        final PsiElement endElement = mock(PsiElement.class);
        when(psiField.getFirstChild()).thenReturn(startElement);
        when(psiField.getLastChild()).thenReturn(endElement);
        assertEquals(Pair.create(startElement, endElement), findPsiFieldWithoutAnnotationBoundsService.findPsiFieldWithoutAnnotationsBounds(psiField));
    }
    @Test
    public void testModifyerNoAnnotation() throws Exception {
        final PsiModifierList psiModifierList = mock(PsiModifierList.class);
        final PsiElement startElement = mock(PsiElement.class);
        final PsiElement endElement = mock(PsiElement.class);
        when(psiField.getFirstChild()).thenReturn(psiModifierList);
        when(psiModifierList.getFirstChild()).thenReturn(startElement);
        when(psiField.getLastChild()).thenReturn(endElement);
        assertEquals(Pair.create(startElement, endElement), findPsiFieldWithoutAnnotationBoundsService.findPsiFieldWithoutAnnotationsBounds(psiField));
    }
    @Test
    public void testModifyerSkipFirstAnnotations() throws Exception {
        final PsiModifierList psiModifierList = mock(PsiModifierList.class);
        final PsiAnnotation annotationElement = mock(PsiAnnotation.class);
        final PsiElement startElement = mock(PsiElement.class);
        final PsiElement endElement = mock(PsiElement.class);
        when(psiField.getFirstChild()).thenReturn(psiModifierList);
        when(psiModifierList.getFirstChild()).thenReturn(annotationElement);
        when(annotationElement.getNextSibling()).thenReturn(startElement);
        when(psiField.getLastChild()).thenReturn(endElement);
        assertEquals(Pair.create(startElement, endElement), findPsiFieldWithoutAnnotationBoundsService.findPsiFieldWithoutAnnotationsBounds(psiField));
    }

    @Test
    public void testModifyerSkipFirstOnlyAnnotations() throws Exception {
        final PsiModifierList psiModifierList = mock(PsiModifierList.class);
        final PsiAnnotation annotationElement = mock(PsiAnnotation.class);
        final PsiElement startElement = mock(PsiElement.class);
        final PsiElement endElement = mock(PsiElement.class);
        when(psiField.getFirstChild()).thenReturn(psiModifierList);
        when(psiModifierList.getFirstChild()).thenReturn(annotationElement);
        when(psiModifierList.getNextSibling()).thenReturn(startElement);
        when(psiField.getLastChild()).thenReturn(endElement);
        assertEquals(Pair.create(startElement, endElement), findPsiFieldWithoutAnnotationBoundsService.findPsiFieldWithoutAnnotationsBounds(psiField));
    }

    @Test
    public void testModifyerSkipFirstWhitespace() throws Exception {
        final PsiModifierList psiModifierList = mock(PsiModifierList.class);
        final PsiWhiteSpace whiteSpace = mock(PsiWhiteSpace.class);
        final PsiElement startElement = mock(PsiElement.class);
        final PsiElement endElement = mock(PsiElement.class);
        when(psiField.getFirstChild()).thenReturn(psiModifierList);
        when(psiModifierList.getFirstChild()).thenReturn(whiteSpace);
        when(whiteSpace.getNextSibling()).thenReturn(startElement);
        when(psiField.getLastChild()).thenReturn(endElement);
        assertEquals(Pair.create(startElement, endElement), findPsiFieldWithoutAnnotationBoundsService.findPsiFieldWithoutAnnotationsBounds(psiField));
    }
}