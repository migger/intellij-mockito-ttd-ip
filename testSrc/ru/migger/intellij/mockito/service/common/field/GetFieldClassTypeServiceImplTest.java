package ru.migger.intellij.mockito.service.common.field;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiPrimitiveType;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetFieldClassTypeServiceImplTest {
    private GetFieldClassTypeService getFieldClassTypeService;

    @Before
    public void setUp() throws Exception {
        getFieldClassTypeService = new GetFieldClassTypeServiceImpl();
    }

    @Test
    public void testIsApplicationComponent() throws Exception {
        assertTrue(getFieldClassTypeService instanceof ApplicationComponent);
    }

    @Test
    public void testGetClassType() throws Exception {
        final PsiClass psiClass = mock(PsiClass.class);
        final Project project = mock(Project.class);
        final PsiField psiField = mock(PsiField.class);
        final PsiClassType psiClassType = mock(PsiClassType.class);
        when(psiClassType.resolve()).thenReturn(psiClass);
        when(psiField.getType()).thenReturn(psiClassType);
        assertEquals(psiClass, getFieldClassTypeService.getClassType(project, psiField));
    }

    @Test
    public void testGetNotClassType() throws Exception {
        final Project project = mock(Project.class);
        final PsiField psiField = mock(PsiField.class);
        final PsiPrimitiveType psiClassType = mock(PsiPrimitiveType.class);
        when(psiField.getType()).thenReturn(psiClassType);
        assertNull(getFieldClassTypeService.getClassType(project, psiField));
    }
}