package ru.migger.intellij.mockito.service.common.field;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiModifierList;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CheckFieldAnnotationServiceImplTest {
    private CheckFieldAnnotationService checkFieldAnnotationService;

    @Before
    public void setUp() throws Exception {
        checkFieldAnnotationService = new CheckFieldAnnotationServiceImpl();
    }

    @Test
    public void testIsApplicationComponent() throws Exception {
        assertTrue(checkFieldAnnotationService instanceof ApplicationComponent);
    }

    @Test
    public void testIsAnnotated() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final PsiModifierList psiModifierList = mock(PsiModifierList.class);
        when(psiField.getModifierList()).thenReturn(psiModifierList);
        final PsiAnnotation psiAnnotation = mock(PsiAnnotation.class);
        when(psiModifierList.getAnnotations()).thenReturn(new PsiAnnotation[]{
                psiAnnotation
        });
        when(psiAnnotation.getQualifiedName()).thenReturn("org.mockito.Mock");
        assertTrue(checkFieldAnnotationService.isFieldAnnotated(psiField, "org.mockito.Mock"));
    }

    @Test
    public void testAnotherAnnotation() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final PsiModifierList psiModifierList = mock(PsiModifierList.class);
        when(psiField.getModifierList()).thenReturn(psiModifierList);
        final PsiAnnotation psiAnnotation = mock(PsiAnnotation.class);
        when(psiModifierList.getAnnotations()).thenReturn(new PsiAnnotation[]{
                psiAnnotation
        });
        when(psiAnnotation.getQualifiedName()).thenReturn("org.jetbrains.annotations.NotNull");
        assertFalse(checkFieldAnnotationService.isFieldAnnotated(psiField, "org.mockito.Mock"));
    }


    @Test
    public void testNoAnnotations() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final PsiModifierList psiModifierList = mock(PsiModifierList.class);
        when(psiField.getModifierList()).thenReturn(psiModifierList);
        when(psiModifierList.getAnnotations()).thenReturn(new PsiAnnotation[0]);
        assertFalse(checkFieldAnnotationService.isFieldAnnotated(psiField, "org.mockito.Mock"));
    }

    @Test
    public void testGetModifierListReturnsNull() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final PsiAnnotation psiAnnotation = mock(PsiAnnotation.class);
        when(psiField.getModifierList()).thenReturn(null);
        when(psiAnnotation.getQualifiedName()).thenReturn("org.mockito.Mock");
        assertFalse(checkFieldAnnotationService.isFieldAnnotated(psiField, "org.mockito.Mock"));
    }
}