package ru.migger.intellij.mockito.service.common.modify;

import com.intellij.openapi.application.Application;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiField;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import ru.migger.intellij.mockito.testutil.RunnableHolder;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ApplicationManager.class)
public class AddFieldServiceImplTest {
    private AddFieldService addFieldService;
    private Application application;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(ApplicationManager.class);
        application = mock(Application.class);
        addFieldService = new AddFieldServiceImpl();
        when(ApplicationManager.getApplication()).thenReturn(application);
    }

    @Test
    public void testIsApplicationComponent() throws Exception {
        assertTrue(addFieldService instanceof ApplicationComponent);
    }

    @Test
    public void testNoField() throws Exception {
        final PsiClass psiClass = mock(PsiClass.class);
        final PsiField field = mock(PsiField.class);
        final RunnableHolder runnableHolder = RunnableHolder.getApplicationWriteActionHolder(application);
        final PsiElement firstChild = mock(PsiElement.class);
        when(psiClass.getFields()).thenReturn(new PsiField[0]);
        when(psiClass.getChildren()).thenReturn(new PsiElement[]{firstChild});
        addFieldService.addField(psiClass, field);
        assertNotNull(runnableHolder.getRunnable());
        verify(psiClass, never()).addBefore(any(), any());
        runnableHolder.getRunnable().run();
        verify(psiClass).add(field);
    }

    @Test
    public void testField() throws Exception {
        final PsiClass psiClass = mock(PsiClass.class);
        final PsiField field = mock(PsiField.class);
        final RunnableHolder runnableHolder = RunnableHolder.getApplicationWriteActionHolder(application);
        final PsiField existingField = mock(PsiField.class);
        when(psiClass.getFields()).thenReturn(new PsiField[]{existingField});
        when(psiClass.getChildren()).thenReturn(new PsiElement[]{existingField});
        addFieldService.addField(psiClass, field);
        assertNotNull(runnableHolder.getRunnable());
        verify(psiClass, never()).addAfter(any(), any());
        runnableHolder.getRunnable().run();
        verify(psiClass).addAfter(field, existingField);
    }
}