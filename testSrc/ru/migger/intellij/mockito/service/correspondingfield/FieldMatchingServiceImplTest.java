package ru.migger.intellij.mockito.service.correspondingfield;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiType;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FieldMatchingServiceImplTest {
    private FieldMatchingService fieldMatchingService;

    @Before
    public void setUp() throws Exception {
        fieldMatchingService = new FieldMatchingServiceImpl();
    }

    @Test
    public void testIsApplicationComponent() throws Exception {
        assertTrue(fieldMatchingService instanceof ApplicationComponent);
    }

    @Test
    public void testTotallyMatched() throws Exception {
        final PsiField psiField1 = mock(PsiField.class);
        final PsiField psiField2 = mock(PsiField.class);
        when(psiField1.getName()).thenReturn("superPuperField");
        when(psiField2.getName()).thenReturn("superPuperField");
        final PsiType psiType = mock(PsiType.class);
        when(psiField1.getType()).thenReturn(psiType);
        when(psiField2.getType()).thenReturn(psiType);
        assertTrue(fieldMatchingService.isFieldsMatched(psiField1, psiField2));
    }

    @Test
    public void testFieldMissmatched() throws Exception {
        final PsiField psiField1 = mock(PsiField.class);
        final PsiField psiField2 = mock(PsiField.class);
        when(psiField1.getName()).thenReturn("superPuperField");
        when(psiField2.getName()).thenReturn("superPuperField");
        final PsiType psiType1 = mock(PsiType.class);
        final PsiType psiType2 = mock(PsiType.class);
        when(psiField1.getType()).thenReturn(psiType1);
        when(psiField2.getType()).thenReturn(psiType2);
        assertFalse(fieldMatchingService.isFieldsMatched(psiField1, psiField2));
    }

    @Test
    public void testNameMismatch() throws Exception {
        final PsiField psiField1 = mock(PsiField.class);
        final PsiField psiField2 = mock(PsiField.class);
        when(psiField1.getName()).thenReturn("superPuperField1");
        when(psiField2.getName()).thenReturn("superPuperField2");
        final PsiType psiType = mock(PsiType.class);
        when(psiField1.getType()).thenReturn(psiType);
        when(psiField2.getType()).thenReturn(psiType);
        assertFalse(fieldMatchingService.isFieldsMatched(psiField1, psiField2));
    }

    @Test
    public void testName1Missed() throws Exception {
        final PsiField psiField1 = mock(PsiField.class);
        final PsiField psiField2 = mock(PsiField.class);
        when(psiField2.getName()).thenReturn("superPuperField2");
        final PsiType psiType = mock(PsiType.class);
        when(psiField1.getType()).thenReturn(psiType);
        when(psiField2.getType()).thenReturn(psiType);
        assertFalse(fieldMatchingService.isFieldsMatched(psiField1, psiField2));
    }

    @Test
    public void testName2Missed() throws Exception {
        final PsiField psiField1 = mock(PsiField.class);
        final PsiField psiField2 = mock(PsiField.class);
        when(psiField1.getName()).thenReturn("superPuperField1");
        final PsiType psiType = mock(PsiType.class);
        when(psiField1.getType()).thenReturn(psiType);
        when(psiField2.getType()).thenReturn(psiType);
        assertFalse(fieldMatchingService.isFieldsMatched(psiField1, psiField2));
    }

    @Test
    public void testNamesMissed() throws Exception {
        final PsiField psiField1 = mock(PsiField.class);
        final PsiField psiField2 = mock(PsiField.class);
        final PsiType psiType = mock(PsiType.class);
        when(psiField1.getType()).thenReturn(psiType);
        when(psiField2.getType()).thenReturn(psiType);
        assertFalse(fieldMatchingService.isFieldsMatched(psiField1, psiField2));
    }
}