package ru.migger.intellij.mockito.service.correspondingfield;

import com.intellij.codeInspection.InspectionManager;
import com.intellij.codeInspection.LocalQuickFix;
import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.util.Pair;
import com.intellij.psi.*;
import org.junit.Before;
import org.junit.Test;
import ru.migger.intellij.mockito.service.common.field.FindPsiFieldWithoutAnnotationBoundsService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MissedCorrespondingRealFieldProblemDescriptorCreateServiceImplTest {
    private MissedCorrespondingRealFieldQuickFixCreateService missedCorrespondingRealFieldQuickFixCreateService;
    private MissedCorrespondingRealFieldProblemDescriptorCreateService missedCorrespondingRealFieldProblemDescriptorCreateService;
    private FindPsiFieldWithoutAnnotationBoundsService findPsiFieldWithoutAnnotationBoundsService;

    private PsiType psiType;
    private PsiClass psiClass;
    private PsiField psiField;
    private InspectionManager inspectionManager;
    private ProblemDescriptor problemDescriptor;
    private LocalQuickFix localQuickFixes;
    private Pair<PsiElement, PsiElement> elementBound;
    private PsiElement fieldStart;
    private PsiElement fieldEnd;

    @Before
    public void setUp() throws Exception {
        psiClass = mock(PsiClass.class);
        psiField = mock(PsiField.class);
        inspectionManager = mock(InspectionManager.class);
        problemDescriptor = mock(ProblemDescriptor.class);
        psiType = mock(PsiType.class);
        localQuickFixes = mock(LocalQuickFix.class);

        elementBound = Pair.create(fieldStart = mock(PsiElement.class), fieldEnd = mock(PsiElement.class));

        missedCorrespondingRealFieldQuickFixCreateService = mock(MissedCorrespondingRealFieldQuickFixCreateService.class);
        findPsiFieldWithoutAnnotationBoundsService = mock(FindPsiFieldWithoutAnnotationBoundsService.class);
        missedCorrespondingRealFieldProblemDescriptorCreateService = new MissedCorrespondingRealFieldProblemDescriptorCreateServiceImpl(
                missedCorrespondingRealFieldQuickFixCreateService,
                findPsiFieldWithoutAnnotationBoundsService
        );

        when(findPsiFieldWithoutAnnotationBoundsService.findPsiFieldWithoutAnnotationsBounds(psiField)).thenReturn(elementBound);
    }

    @Test
    public void testIsApplicationComponent() throws Exception {
        assertTrue(missedCorrespondingRealFieldProblemDescriptorCreateService instanceof ApplicationComponent);
    }

    @Test
    public void testValidDescriptor() throws Exception {
        when(psiClass.getName()).thenReturn("className");
        when(psiField.getName()).thenReturn("fieldName");
        when(psiField.getType()).thenReturn(psiType);
        when(psiType.getCanonicalText()).thenReturn("typeName");
        when(missedCorrespondingRealFieldQuickFixCreateService.createLocalQuickFixes(psiField, psiClass)).thenReturn(localQuickFixes);
        when(inspectionManager.createProblemDescriptor(
                fieldStart, fieldEnd,
                "Class className have no filed 'typeName fieldName'",
                ProblemHighlightType.GENERIC_ERROR,
                true,
                localQuickFixes)).thenReturn(problemDescriptor);
        assertEquals(problemDescriptor, missedCorrespondingRealFieldProblemDescriptorCreateService.createProblemDescriptors(psiField, psiClass, inspectionManager));
    }

    @Test
    public void testFactoryReturnsNull() throws Exception {
        when(psiClass.getName()).thenReturn("className");
        when(psiField.getName()).thenReturn("fieldName");
        when(psiField.getType()).thenReturn(psiType);
        when(psiType.getCanonicalText()).thenReturn("typeName");
        when(missedCorrespondingRealFieldQuickFixCreateService.createLocalQuickFixes(psiField, psiClass)).thenReturn(null);
        when(inspectionManager.createProblemDescriptor(
                fieldStart, fieldEnd,
                "Class className have no filed 'typeName fieldName'",
                ProblemHighlightType.GENERIC_ERROR,
                true)).thenReturn(problemDescriptor);
        assertEquals(problemDescriptor, missedCorrespondingRealFieldProblemDescriptorCreateService.createProblemDescriptors(psiField, psiClass, inspectionManager));
    }


}