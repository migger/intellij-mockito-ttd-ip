package ru.migger.intellij.mockito.service.correspondingfield.quickfix;

import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import ru.migger.intellij.mockito.service.common.aclass.FindPsiClassService;
import ru.migger.intellij.mockito.service.common.field.GetFieldClassTypeService;
import ru.migger.intellij.mockito.service.common.modify.AddFieldService;
import ru.migger.intellij.mockito.service.common.modify.EnsureClassesIsImportedService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class MissedCorrespondingFieldLocalQuickFixTest {
    private MissedCorrespondingFieldLocalQuickFix missedCorrespondingFieldLocalQuickFix;
    private EnsureClassesIsImportedService ensureClassesIsImportedService;
    private FindPsiClassService findPsiClassService;
    private GetFieldClassTypeService getFieldClassTypeService;
    private AddFieldService addFieldService;
    private PsiField psiField;
    private PsiClass psiClass;
    private Project project;

    @Before
    public void setUp() throws Exception {
        psiField = mock(PsiField.class);
        psiClass = mock(PsiClass.class);
        project = mock(Project.class);
        addFieldService = mock(AddFieldService.class);
        getFieldClassTypeService = mock(GetFieldClassTypeService.class);
        findPsiClassService = mock(FindPsiClassService.class);
        ensureClassesIsImportedService = mock(EnsureClassesIsImportedService.class);
        missedCorrespondingFieldLocalQuickFix = MissedCorrespondingFieldLocalQuickFix.create(
                psiField, psiClass, ensureClassesIsImportedService, findPsiClassService, getFieldClassTypeService, addFieldService);
    }

    @Test
    public void testGetName() throws Exception {
        final PsiType psiType = mock(PsiType.class);
        when(psiField.getName()).thenReturn("fieldName");
        when(psiField.getType()).thenReturn(psiType);
        when(psiType.getCanonicalText()).thenReturn("type");
        when(psiClass.getName()).thenReturn("className");
        assertEquals("Add 'type fieldName' to class className", missedCorrespondingFieldLocalQuickFix.getName());
    }

    @Test
    public void testGetFamilyName() throws Exception {
        assertEquals("MockitoTTD", missedCorrespondingFieldLocalQuickFix.getFamilyName());
    }

    @Test
    public void testApplyFix() throws Exception {
        final PsiJavaFile psiFile = mock(PsiJavaFile.class);
        final PsiClass autorwiredClass = mock(PsiClass.class);
        final PsiClass fieldClassType = mock(PsiClass.class);
        final PsiField fieldCopy = mock(PsiField.class);
        final PsiModifierList copyFieldModifierList = mock(PsiModifierList.class);
        when(findPsiClassService.findClassByFullName(project, "org.springframework.beans.factory.annotation.Autowired")).thenReturn(autorwiredClass);
        when(getFieldClassTypeService.getClassType(project, psiField)).thenReturn(fieldClassType);
        when(psiClass.getContainingFile()).thenReturn(psiFile);
        when(psiField.copy()).thenReturn(fieldCopy);
        when(fieldCopy.getModifierList()).thenReturn(copyFieldModifierList);
        final PsiAnnotation copyAnnotation = mock(PsiAnnotation.class);
        when(copyFieldModifierList.getAnnotations()).thenReturn(new PsiAnnotation[]{copyAnnotation});
        missedCorrespondingFieldLocalQuickFix.applyFix(project, mock(ProblemDescriptor.class));
        final InOrder order = inOrder(copyFieldModifierList);
        order.verify(copyFieldModifierList).deleteChildRange(copyAnnotation, copyAnnotation);
        order.verify(copyFieldModifierList).addAnnotation("Autowired");
        verify(copyFieldModifierList).setModifierProperty("private", true);
        verify(copyFieldModifierList).setModifierProperty("public", false);
        verify(copyFieldModifierList).setModifierProperty("protected", false);
        verify(addFieldService).addField(psiClass, fieldCopy);
        verify(ensureClassesIsImportedService).ensureIsImported(psiFile, autorwiredClass);
        verify(ensureClassesIsImportedService).ensureIsImported(psiFile, fieldClassType);
    }

    @Test
    public void testNoAutowired() throws Exception {
        final PsiJavaFile psiFile = mock(PsiJavaFile.class);
        final PsiClass fieldClassType = mock(PsiClass.class);
        when(findPsiClassService.findClassByFullName(project, "org.springframework.beans.factory.annotation.Autowired")).thenReturn(null);
        when(getFieldClassTypeService.getClassType(project, psiField)).thenReturn(fieldClassType);
        when(psiClass.getContainingFile()).thenReturn(psiFile);
        missedCorrespondingFieldLocalQuickFix.applyFix(project, mock(ProblemDescriptor.class));
        verify(addFieldService, never()).addField(any(), any());
        verify(ensureClassesIsImportedService, never()).ensureIsImported(eq(psiFile), any());
    }

    @Test
    public void testNoModifiedList() throws Exception {
        final PsiJavaFile psiFile = mock(PsiJavaFile.class);
        final PsiClass autowired = mock(PsiClass.class);
        final PsiClass fieldClassType = mock(PsiClass.class);
        when(findPsiClassService.findClassByFullName(project, "org.springframework.beans.factory.annotation.Autowired")).thenReturn(autowired);
        when(getFieldClassTypeService.getClassType(project, psiField)).thenReturn(fieldClassType);
        when(psiClass.getContainingFile()).thenReturn(psiFile);
        when(psiField.copy()).thenReturn(mock(PsiField.class));
        missedCorrespondingFieldLocalQuickFix.applyFix(project, mock(ProblemDescriptor.class));
        verify(addFieldService, never()).addField(any(), any());
        verify(ensureClassesIsImportedService, never()).ensureIsImported(eq(psiFile), any());
    }

    @Test
    public void testNoFieldClass() throws Exception {
        final PsiJavaFile psiFile = mock(PsiJavaFile.class);
        final PsiClass autowiredClass = mock(PsiClass.class);
        when(findPsiClassService.findClassByFullName(project, "org.springframework.beans.factory.annotation.Autowired")).thenReturn(autowiredClass);
        when(getFieldClassTypeService.getClassType(project, psiField)).thenReturn(null);
        when(psiClass.getContainingFile()).thenReturn(psiFile);
        missedCorrespondingFieldLocalQuickFix.applyFix(project, mock(ProblemDescriptor.class));
        verify(addFieldService, never()).addField(any(), any());
        verify(ensureClassesIsImportedService, never()).ensureIsImported(eq(psiFile), any());
    }
}