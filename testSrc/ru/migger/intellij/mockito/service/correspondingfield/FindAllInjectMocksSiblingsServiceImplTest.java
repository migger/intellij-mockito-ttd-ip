package ru.migger.intellij.mockito.service.correspondingfield;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import org.junit.Before;
import org.junit.Test;
import ru.migger.intellij.mockito.service.common.field.CheckFieldAnnotationService;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FindAllInjectMocksSiblingsServiceImplTest {
    private FindAllInjectMocksSiblingsService findAllInjectMocksSiblingsService;
    private CheckFieldAnnotationService checkFieldAnnotationService;


    @Before
    public void setUp() throws Exception {
        checkFieldAnnotationService = mock(CheckFieldAnnotationService.class);
        findAllInjectMocksSiblingsService = new FindAllInjectMocksSiblingsServiceImpl(checkFieldAnnotationService);
    }

    @Test
    public void testIsApplicationComponent() throws Exception {
        assertTrue(findAllInjectMocksSiblingsService instanceof ApplicationComponent);
    }

    @Test
    public void testFindAllSiblings() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final PsiClass psiClass = mock(PsiClass.class);
        final PsiField sibling = mock(PsiField.class);
        when(psiField.getContainingClass()).thenReturn(psiClass);
        when(psiClass.getFields()).thenReturn(new PsiField[]{
                sibling,
                psiField
        });
        when(checkFieldAnnotationService.isFieldAnnotated(sibling, "org.mockito.InjectMocks")).thenReturn(true);
        assertArrayEquals(new PsiField[]{sibling}, findAllInjectMocksSiblingsService.findAllInjectMocksSiblings(psiField));
    }

    @Test
    public void testNotAnnotatedSibling() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final PsiClass psiClass = mock(PsiClass.class);
        final PsiField sibling = mock(PsiField.class);
        final PsiField notAnnotatedSibling = mock(PsiField.class);
        when(psiField.getContainingClass()).thenReturn(psiClass);
        when(psiClass.getFields()).thenReturn(new PsiField[]{
                sibling,
                notAnnotatedSibling,
                psiField
        });
        when(checkFieldAnnotationService.isFieldAnnotated(sibling, "org.mockito.InjectMocks")).thenReturn(true);
        when(checkFieldAnnotationService.isFieldAnnotated(notAnnotatedSibling, "org.mockito.InjectMocks")).thenReturn(false);
        assertArrayEquals(new PsiField[]{sibling}, findAllInjectMocksSiblingsService.findAllInjectMocksSiblings(psiField));
    }

    @Test
    public void testTwoAnnotatedSiblings() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final PsiClass psiClass = mock(PsiClass.class);
        final PsiField sibling = mock(PsiField.class);
        final PsiField sibling2 = mock(PsiField.class);
        when(psiField.getContainingClass()).thenReturn(psiClass);
        when(psiClass.getFields()).thenReturn(new PsiField[]{
                sibling,
                sibling2,
                psiField
        });
        when(checkFieldAnnotationService.isFieldAnnotated(sibling, "org.mockito.InjectMocks")).thenReturn(true);
        when(checkFieldAnnotationService.isFieldAnnotated(sibling2, "org.mockito.InjectMocks")).thenReturn(true);
        assertArrayEquals(new PsiField[]{sibling, sibling2}, findAllInjectMocksSiblingsService.findAllInjectMocksSiblings(psiField));
    }

    @Test
    public void testContainingClassIsNull() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final PsiClass psiClass = mock(PsiClass.class);
        final PsiField sibling = mock(PsiField.class);
        assertArrayEquals(new PsiField[]{}, findAllInjectMocksSiblingsService.findAllInjectMocksSiblings(psiField));
    }
}