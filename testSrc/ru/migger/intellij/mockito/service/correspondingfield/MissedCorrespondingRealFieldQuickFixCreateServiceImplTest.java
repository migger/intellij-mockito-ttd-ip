package ru.migger.intellij.mockito.service.correspondingfield;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import ru.migger.intellij.mockito.service.common.aclass.FindPsiClassService;
import ru.migger.intellij.mockito.service.common.field.GetFieldClassTypeService;
import ru.migger.intellij.mockito.service.common.modify.AddFieldService;
import ru.migger.intellij.mockito.service.common.modify.EnsureClassesIsImportedService;
import ru.migger.intellij.mockito.service.correspondingfield.quickfix.MissedCorrespondingFieldLocalQuickFix;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(MissedCorrespondingFieldLocalQuickFix.class)

public class MissedCorrespondingRealFieldQuickFixCreateServiceImplTest {
    private MissedCorrespondingRealFieldQuickFixCreateService missedCorrespondingRealFieldQuickFixCreateService;
    private EnsureClassesIsImportedService ensureClassesIsImportedService;
    private FindPsiClassService findPsiClassService;
    private GetFieldClassTypeService getFieldClassTypeService;
    private AddFieldService addFieldService;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(MissedCorrespondingFieldLocalQuickFix.class);
        ensureClassesIsImportedService = mock(EnsureClassesIsImportedService.class);
        findPsiClassService = mock(FindPsiClassService.class);
        getFieldClassTypeService = mock(GetFieldClassTypeService.class);
        addFieldService = mock(AddFieldService.class);
        missedCorrespondingRealFieldQuickFixCreateService = new MissedCorrespondingRealFieldQuickFixCreateServiceImpl(
                ensureClassesIsImportedService,
                findPsiClassService,
                getFieldClassTypeService,
                addFieldService
        );
    }

    @Test
    public void testIsApplicationComponent() throws Exception {
        assertTrue(missedCorrespondingRealFieldQuickFixCreateService instanceof ApplicationComponent);
    }

    @Test
    public void testCreated() throws Exception {
        final MissedCorrespondingFieldLocalQuickFix quickFix = mock(MissedCorrespondingFieldLocalQuickFix.class);
        final PsiClass psiClass = mock(PsiClass.class);
        final PsiField psiField = mock(PsiField.class);
        final Project project = mock(Project.class);
        when(psiClass.getProject()).thenReturn(project);
        when(findPsiClassService.findClassByFullName(project, "org.springframework.beans.factory.annotation.Autowired")).thenReturn(mock(PsiClass.class));
        when(MissedCorrespondingFieldLocalQuickFix.create(psiField, psiClass,
                ensureClassesIsImportedService, findPsiClassService,
                getFieldClassTypeService, addFieldService)).thenReturn(quickFix);
        assertEquals(quickFix, missedCorrespondingRealFieldQuickFixCreateService.createLocalQuickFixes(psiField, psiClass));
    }

    @Test
    public void testNotCreatedWhenNoAutowired() throws Exception {
        final MissedCorrespondingFieldLocalQuickFix quickFix = mock(MissedCorrespondingFieldLocalQuickFix.class);
        final PsiClass psiClass = mock(PsiClass.class);
        final PsiField psiField = mock(PsiField.class);
        when(MissedCorrespondingFieldLocalQuickFix.create(psiField, psiClass,
                ensureClassesIsImportedService, findPsiClassService,
                getFieldClassTypeService, addFieldService)).thenReturn(quickFix);
        assertNull(missedCorrespondingRealFieldQuickFixCreateService.createLocalQuickFixes(psiField, psiClass));
    }
}