package ru.migger.intellij.mockito.service.correspondingfield;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CheckMockFieldHaveCorrespondingRealFieldServiceImplTest {
    private CheckMockFieldHaveCorrespondingRealFieldService checkMockFieldHaveCorrespondingRealFieldService;
    private FieldMatchingService fieldMatchingService;


    @Before
    public void setUp() throws Exception {
        fieldMatchingService = mock(FieldMatchingService.class);
        checkMockFieldHaveCorrespondingRealFieldService = new CheckMockFieldHaveCorrespondingRealFieldServiceImpl(fieldMatchingService);
    }

    @Test
    public void testIsApplicationComponent() throws Exception {
        assertTrue(checkMockFieldHaveCorrespondingRealFieldService instanceof ApplicationComponent);
    }

    @Test
    public void testWorks() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final PsiClass psiClass = mock(PsiClass.class);
        final PsiField classField1 = mock(PsiField.class);
        final PsiField classField2 = mock(PsiField.class);
        final PsiField classField3 = mock(PsiField.class);
        when(psiClass.getFields()).thenReturn(new PsiField[]{
                classField1,
                classField2,
                classField3
        });
        when(fieldMatchingService.isFieldsMatched(psiField, classField1)).thenReturn(false);
        when(fieldMatchingService.isFieldsMatched(psiField, classField2)).thenReturn(true);
        when(fieldMatchingService.isFieldsMatched(psiField, classField3)).thenReturn(false);

        assertTrue(checkMockFieldHaveCorrespondingRealFieldService.isMockFieldHaveCorrespondingRealField(psiField, psiClass));

    }

    @Test
    public void testNotMatched() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final PsiClass psiClass = mock(PsiClass.class);
        final PsiField classField1 = mock(PsiField.class);
        final PsiField classField2 = mock(PsiField.class);
        final PsiField classField3 = mock(PsiField.class);
        when(psiClass.getFields()).thenReturn(new PsiField[]{
                classField1,
                classField2,
                classField3
        });
        when(fieldMatchingService.isFieldsMatched(psiField, classField1)).thenReturn(false);
        when(fieldMatchingService.isFieldsMatched(psiField, classField2)).thenReturn(false);
        when(fieldMatchingService.isFieldsMatched(psiField, classField3)).thenReturn(false);

        assertFalse(checkMockFieldHaveCorrespondingRealFieldService.isMockFieldHaveCorrespondingRealField(psiField, psiClass));

    }
}