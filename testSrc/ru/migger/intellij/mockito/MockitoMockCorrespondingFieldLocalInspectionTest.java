package ru.migger.intellij.mockito;

import com.intellij.codeInspection.InspectionManager;
import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiType;
import org.junit.Before;
import org.junit.Test;
import ru.migger.intellij.mockito.service.common.field.CheckFieldAnnotationService;
import ru.migger.intellij.mockito.service.common.field.FindPsiFieldInitializerClassService;
import ru.migger.intellij.mockito.service.correspondingfield.CheckMockFieldHaveCorrespondingRealFieldService;
import ru.migger.intellij.mockito.service.correspondingfield.FindAllInjectMocksSiblingsService;
import ru.migger.intellij.mockito.service.correspondingfield.MissedCorrespondingRealFieldProblemDescriptorCreateService;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MockitoMockCorrespondingFieldLocalInspectionTest {
    private MockitoMockCorrespondingFieldLocalInspection mockitoMockCorrespondingFieldLocalInspection;
    private CheckFieldAnnotationService checkFieldAnnotationService;
    private CheckMockFieldHaveCorrespondingRealFieldService checkMockFieldHaveCorrespondingRealFieldService;
    private FindAllInjectMocksSiblingsService findAllInjectMocksSiblingsService;
    private FindPsiFieldInitializerClassService findPsiClassService;
    private MissedCorrespondingRealFieldProblemDescriptorCreateService missedCorrespondingRealFieldProblemDescriptorCreateService;


    @Before
    public void setUp() throws Exception {
        missedCorrespondingRealFieldProblemDescriptorCreateService = mock(MissedCorrespondingRealFieldProblemDescriptorCreateService.class);
        findPsiClassService = mock(FindPsiFieldInitializerClassService.class);
        checkFieldAnnotationService = mock(CheckFieldAnnotationService.class);
        checkMockFieldHaveCorrespondingRealFieldService = mock(CheckMockFieldHaveCorrespondingRealFieldService.class);
        findAllInjectMocksSiblingsService = mock(FindAllInjectMocksSiblingsService.class);
        mockitoMockCorrespondingFieldLocalInspection = new MockitoMockCorrespondingFieldLocalInspection(
                checkFieldAnnotationService,
                checkMockFieldHaveCorrespondingRealFieldService,
                findAllInjectMocksSiblingsService,
                findPsiClassService,
                missedCorrespondingRealFieldProblemDescriptorCreateService
        );
    }

    @Test
    public void testWorks() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final InspectionManager inspectionManager = mock(InspectionManager.class);
        final ProblemDescriptor problemDescriptor = mock(ProblemDescriptor.class);
        final ProblemDescriptor[] expecteds = {problemDescriptor};
        final PsiField injectMockSibling = mock(PsiField.class);
        final PsiType injectMocksType = mock(PsiType.class);
        final PsiClass injectMocksClass = mock(PsiClass.class);
        when(injectMocksType.getCanonicalText()).thenReturn("org.java.Type");
        when(injectMockSibling.getType()).thenReturn(injectMocksType);
        when(findPsiClassService.findInitializerClass(injectMockSibling)).thenReturn(injectMocksClass);
        when(checkFieldAnnotationService.isFieldAnnotated(psiField, "org.mockito.Mock")).thenReturn(true);
        when(findAllInjectMocksSiblingsService.findAllInjectMocksSiblings(psiField)).thenReturn(new PsiField[]{injectMockSibling});
        when(checkMockFieldHaveCorrespondingRealFieldService.isMockFieldHaveCorrespondingRealField(psiField, injectMocksClass)).thenReturn(false);
        when(missedCorrespondingRealFieldProblemDescriptorCreateService.createProblemDescriptors(psiField, injectMocksClass, inspectionManager)).thenReturn(problemDescriptor);
        assertArrayEquals(expecteds, mockitoMockCorrespondingFieldLocalInspection.checkField(psiField, inspectionManager, true));
    }

    @Test
    public void testNoSiblings() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final InspectionManager inspectionManager = mock(InspectionManager.class);
        when(findAllInjectMocksSiblingsService.findAllInjectMocksSiblings(psiField)).thenReturn(new PsiField[0]);
        assertNull(mockitoMockCorrespondingFieldLocalInspection.checkField(psiField, inspectionManager, true));
    }

    @Test
    public void testFieldIsNotAnnotatedWithMock() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final InspectionManager inspectionManager = mock(InspectionManager.class);
        final PsiField injectMockSibling = mock(PsiField.class);
        final PsiType injectMocksType = mock(PsiType.class);
        final PsiClass injectMocksClass = mock(PsiClass.class);
        when(injectMockSibling.getType()).thenReturn(injectMocksType);
        when(injectMocksType.getCanonicalText()).thenReturn("org.java.Type");
        when(findPsiClassService.findInitializerClass(psiField)).thenReturn(injectMocksClass);
        when(findAllInjectMocksSiblingsService.findAllInjectMocksSiblings(psiField)).thenReturn(new PsiField[]{injectMockSibling});
        when(checkFieldAnnotationService.isFieldAnnotated(psiField, "org.mockito.Mock")).thenReturn(false
        );
        when(checkMockFieldHaveCorrespondingRealFieldService.isMockFieldHaveCorrespondingRealField(psiField, injectMocksClass)).thenReturn(false);
        when(missedCorrespondingRealFieldProblemDescriptorCreateService.createProblemDescriptors(psiField, injectMocksClass, inspectionManager)).thenReturn(mock(ProblemDescriptor.class));
        assertNull(mockitoMockCorrespondingFieldLocalInspection.checkField(psiField, inspectionManager, true));
    }

    @Test
    public void testNoIjectMocksClassFound() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final InspectionManager inspectionManager = mock(InspectionManager.class);
        final PsiField injectMockSibling = mock(PsiField.class);
        final PsiType injectMocksType = mock(PsiType.class);
        when(injectMockSibling.getType()).thenReturn(injectMocksType);
        when(injectMocksType.getCanonicalText()).thenReturn("org.java.Type");
        when(findAllInjectMocksSiblingsService.findAllInjectMocksSiblings(psiField)).thenReturn(new PsiField[]{injectMockSibling});
        assertNull(mockitoMockCorrespondingFieldLocalInspection.checkField(psiField, inspectionManager, true));
    }

    @Test
    public void testNoIjectMocksClassHaveCorrespondingField() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final InspectionManager inspectionManager = mock(InspectionManager.class);
        final PsiField injectMockSibling = mock(PsiField.class);
        final PsiType injectMocksType = mock(PsiType.class);
        final PsiClass injectMocksClass = mock(PsiClass.class);
        when(injectMockSibling.getType()).thenReturn(injectMocksType);
        when(injectMocksType.getCanonicalText()).thenReturn("org.java.Type");
        when(findPsiClassService.findInitializerClass(psiField)).thenReturn(injectMocksClass);
        when(findAllInjectMocksSiblingsService.findAllInjectMocksSiblings(psiField)).thenReturn(new PsiField[]{injectMockSibling});
        when(checkMockFieldHaveCorrespondingRealFieldService.isMockFieldHaveCorrespondingRealField(psiField, injectMocksClass)).thenReturn(true);
        assertNull(mockitoMockCorrespondingFieldLocalInspection.checkField(psiField, inspectionManager, true));
    }

    @Test
    public void testUnableToCreateProblemDescriptors() throws Exception {
        final PsiField psiField = mock(PsiField.class);
        final InspectionManager inspectionManager = mock(InspectionManager.class);
        final PsiField injectMockSibling = mock(PsiField.class);
        final PsiType injectMocksType = mock(PsiType.class);
        final PsiClass injectMocksClass = mock(PsiClass.class);
        when(injectMockSibling.getType()).thenReturn(injectMocksType);
        when(injectMocksType.getCanonicalText()).thenReturn("org.java.Type");
        when(findPsiClassService.findInitializerClass(psiField)).thenReturn(injectMocksClass);
        when(findAllInjectMocksSiblingsService.findAllInjectMocksSiblings(psiField)).thenReturn(new PsiField[]{injectMockSibling});
        when(checkMockFieldHaveCorrespondingRealFieldService.isMockFieldHaveCorrespondingRealField(psiField, injectMocksClass)).thenReturn(false);
        assertNull(mockitoMockCorrespondingFieldLocalInspection.checkField(psiField, inspectionManager, true));
    }
}