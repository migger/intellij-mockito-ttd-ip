package ru.migger.intellij.mockito.service.common.field;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class GetFieldClassTypeServiceImpl extends ApplicationComponent.Adapter implements GetFieldClassTypeService {
    @Nullable
    @Override
    public PsiClass getClassType(@NotNull Project project, @NotNull PsiField psiField) {
        final PsiType type = psiField.getType();
        if (type instanceof PsiClassType) {
            return ((PsiClassType) type).resolve();
        }
        return null;
    }

}
