package ru.migger.intellij.mockito.service.common.field;

import com.intellij.psi.PsiField;
import org.jetbrains.annotations.NotNull;

public interface CheckFieldAnnotationService {
    boolean isFieldAnnotated(@NotNull PsiField psiField, @NotNull String className);
}
