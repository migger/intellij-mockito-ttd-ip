package ru.migger.intellij.mockito.service.common.field;

import com.intellij.psi.PsiAnnotation;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface GetPsiAnnotationDotClassParameterService {
    @Nullable
    String getQualifiedClassNameOfPsiAnnotationParameter(@NotNull PsiAnnotation psiAnnotation, @NotNull String parameterName);
}
