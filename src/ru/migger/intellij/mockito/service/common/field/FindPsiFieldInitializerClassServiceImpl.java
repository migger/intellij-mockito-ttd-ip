package ru.migger.intellij.mockito.service.common.field;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiExpression;
import com.intellij.psi.PsiField;

public class FindPsiFieldInitializerClassServiceImpl extends ApplicationComponent.Adapter implements FindPsiFieldInitializerClassService {
    @Override
    public PsiClass findInitializerClass(PsiField psiField) {
        final PsiExpression initializer = psiField.getInitializer();
        if (initializer == null)
            return null;
        if (initializer.getType() instanceof PsiClassType) {
            return ((PsiClassType) initializer.getType()).resolve();
        }
        return null;
    }
}
