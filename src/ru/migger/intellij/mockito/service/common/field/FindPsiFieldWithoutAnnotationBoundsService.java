package ru.migger.intellij.mockito.service.common.field;

import com.intellij.openapi.util.Pair;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiField;

public interface FindPsiFieldWithoutAnnotationBoundsService {
    Pair<PsiElement, PsiElement> findPsiFieldWithoutAnnotationsBounds(PsiField psiField);
}
