package ru.migger.intellij.mockito.service.common.field;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;

public interface FindPsiFieldInitializerClassService {
    PsiClass findInitializerClass(PsiField psiField);
}
