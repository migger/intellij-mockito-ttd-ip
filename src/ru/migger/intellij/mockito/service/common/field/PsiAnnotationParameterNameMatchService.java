package ru.migger.intellij.mockito.service.common.field;

import com.intellij.psi.PsiNameValuePair;
import org.jetbrains.annotations.NotNull;

public interface PsiAnnotationParameterNameMatchService {
    boolean isMatched(@NotNull String value, @NotNull PsiNameValuePair psiNameValuePair);
}
