package ru.migger.intellij.mockito.service.common.field;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.util.Pair;
import com.intellij.psi.*;

public class FindPsiFieldWithoutAnnotationBoundsServiceImpl extends ApplicationComponent.Adapter implements FindPsiFieldWithoutAnnotationBoundsService {
    @Override
    public Pair<PsiElement, PsiElement> findPsiFieldWithoutAnnotationsBounds(PsiField psiField) {
        PsiElement firstChild = psiField.getFirstChild();
        if (firstChild instanceof PsiModifierList) {
            firstChild = firstChild.getFirstChild();
            while (true){
                if(firstChild == null) {
                    firstChild = psiField.getFirstChild().getNextSibling();
                }
                if(!(firstChild instanceof PsiAnnotation || firstChild instanceof PsiWhiteSpace))
                    break;
                firstChild = firstChild.getNextSibling();
            }
        }
        return Pair.create(firstChild, psiField.getLastChild());
    }
}
