package ru.migger.intellij.mockito.service.common.field;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiModifierList;
import org.jetbrains.annotations.NotNull;

public class CheckFieldAnnotationServiceImpl extends ApplicationComponent.Adapter implements CheckFieldAnnotationService {
    @Override
    public boolean isFieldAnnotated(@NotNull PsiField psiField, @NotNull String className) {
        final PsiModifierList modifierList = psiField.getModifierList();
        if (modifierList == null) {
            return false;
        }
        for (PsiAnnotation psiAnnotation : modifierList.getAnnotations()) {
            if (className.equals(psiAnnotation.getQualifiedName()))
                return true;
        }
        return false;
    }
}
