package ru.migger.intellij.mockito.service.common.field;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface GetPsiFieldParentClassService {
    @Nullable
    PsiClass getParentClass(@NotNull PsiField psiField);
}
