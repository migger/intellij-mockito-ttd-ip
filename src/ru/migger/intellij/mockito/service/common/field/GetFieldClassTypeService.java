package ru.migger.intellij.mockito.service.common.field;

import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface GetFieldClassTypeService {
    @Nullable
    PsiClass getClassType(@NotNull Project project, @NotNull PsiField psiField);
}
