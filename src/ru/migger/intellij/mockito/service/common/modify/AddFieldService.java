package ru.migger.intellij.mockito.service.common.modify;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;

public interface AddFieldService {
    void addField(PsiClass psiClass, PsiField psiField);
}
