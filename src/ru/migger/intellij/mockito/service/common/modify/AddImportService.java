package ru.migger.intellij.mockito.service.common.modify;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiJavaFile;
import org.jetbrains.annotations.NotNull;

public interface AddImportService {
    void addImportStatement(@NotNull PsiJavaFile psiJavaFile, @NotNull PsiClass toImport);
}
