package ru.migger.intellij.mockito.service.common.modify;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiImportStatement;
import com.intellij.psi.PsiJavaFile;
import org.jetbrains.annotations.NotNull;

public class EnsureClassesIsImportedServiceImpl extends ApplicationComponent.Adapter implements EnsureClassesIsImportedService {
    private final AddImportService addImportService;

    public EnsureClassesIsImportedServiceImpl(AddImportService addImportService) {
        this.addImportService = addImportService;
    }

    @Override
    public void ensureIsImported(@NotNull PsiJavaFile javaFile, @NotNull PsiClass toImport) {
        if (javaFile.getImportList() != null) {
            for (PsiImportStatement psiImportStatement : javaFile.getImportList().getImportStatements()) {
                final String importQualifiedName = psiImportStatement.getQualifiedName();
                if(importQualifiedName != null && importQualifiedName.equals(toImport.getQualifiedName()))
                    return;
            }
        }
        addImportService.addImportStatement(javaFile, toImport);
    }
}
