package ru.migger.intellij.mockito.service.common.modify;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiJavaFile;
import org.jetbrains.annotations.NotNull;

public interface EnsureClassesIsImportedService {
    void ensureIsImported(@NotNull PsiJavaFile javaFile, @NotNull PsiClass first);
}
