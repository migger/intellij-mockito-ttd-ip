package ru.migger.intellij.mockito.service.common.modify;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;

public class AddFieldServiceImpl extends ApplicationComponent.Adapter implements AddFieldService {
    @Override
    public void addField(PsiClass psiClass, PsiField psiField) {
        ApplicationManager.getApplication().runWriteAction(new Runnable() {
            @Override
            public void run() {
                if (psiClass.getFields().length == 0) {
                    psiClass.add(psiField);
                } else {
                    final PsiField[] fields = psiClass.getFields();
                    psiClass.addAfter(psiField, fields[fields.length - 1]);
                }
            }
        });
    }
}
