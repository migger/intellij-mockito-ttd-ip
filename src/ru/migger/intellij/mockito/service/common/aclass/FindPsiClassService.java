package ru.migger.intellij.mockito.service.common.aclass;

import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface FindPsiClassService {
    @Nullable
    PsiClass findClassByFullName(@NotNull Project project, @NotNull String fullName);
}
