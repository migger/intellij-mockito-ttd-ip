package ru.migger.intellij.mockito.service.runwith;

import com.intellij.psi.PsiClass;
import org.jetbrains.annotations.NotNull;

public interface CheckPsiClassHaveProperRunWithAnnotatiosService {
    boolean isPsiClassHaveProperAnnotation(@NotNull PsiClass psiClass);
}
