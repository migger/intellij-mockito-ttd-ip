package ru.migger.intellij.mockito.service.runwith;

import com.intellij.codeInspection.InspectionManager;
import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.util.Pair;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiIdentifier;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.migger.intellij.mockito.service.common.field.FindPsiFieldWithoutAnnotationBoundsService;
import ru.migger.intellij.mockito.service.common.field.GetPsiFieldParentClassService;

public class CreateMissedRunWithAnnotationProblemDescriptorServiceImpl extends ApplicationComponent.Adapter implements CreateMissedRunWithAnnotationProblemDescriptorService {
    private final GetPsiFieldParentClassService getPsiFieldParentClassService;
    private final CreateMissedRunWithAnnotationLocalQuickFixService createMissedRunWithAnnotationLocalQuickFixService;
    private final FindPsiFieldWithoutAnnotationBoundsService findPsiFieldWithoutAnnotationBoundsService;

    public CreateMissedRunWithAnnotationProblemDescriptorServiceImpl(GetPsiFieldParentClassService getPsiFieldParentClassService, CreateMissedRunWithAnnotationLocalQuickFixService createMissedRunWithAnnotationLocalQuickFixService, FindPsiFieldWithoutAnnotationBoundsService findPsiFieldWithoutAnnotationBoundsService) {
        this.getPsiFieldParentClassService = getPsiFieldParentClassService;
        this.createMissedRunWithAnnotationLocalQuickFixService = createMissedRunWithAnnotationLocalQuickFixService;
        this.findPsiFieldWithoutAnnotationBoundsService = findPsiFieldWithoutAnnotationBoundsService;
    }

    @Nullable
    @Override
    public ProblemDescriptor createProblemDescriptor(@NotNull InspectionManager inspectionManager, @NotNull PsiField psiField) {
        final Pair<PsiElement, PsiElement> bounds = findPsiFieldWithoutAnnotationBoundsService.findPsiFieldWithoutAnnotationsBounds(psiField);
        return inspectionManager.createProblemDescriptor(
                bounds.getFirst(), bounds.getSecond(),
                "Class is not annotated with @RunWith(MockitoJUnitRunner.class)", ProblemHighlightType.GENERIC_ERROR, true,
                createMissedRunWithAnnotationLocalQuickFixService.createLocalQuickFixes(getPsiFieldParentClassService.getParentClass(psiField)));
    }
}
