package ru.migger.intellij.mockito.service.runwith;

import com.intellij.psi.PsiField;
import org.jetbrains.annotations.NotNull;

public interface CheckPsiFieldHaveMockitoAnnotationService {
    boolean isPsiFiledHaveMockitoAnnotation(@NotNull PsiField psiField);
}
