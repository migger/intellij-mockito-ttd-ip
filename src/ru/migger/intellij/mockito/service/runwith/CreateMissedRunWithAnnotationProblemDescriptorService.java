package ru.migger.intellij.mockito.service.runwith;

import com.intellij.codeInspection.InspectionManager;
import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.psi.PsiField;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface CreateMissedRunWithAnnotationProblemDescriptorService {
    @Nullable
    ProblemDescriptor createProblemDescriptor(@NotNull InspectionManager inspectionManager, @NotNull PsiField psiField);
}
