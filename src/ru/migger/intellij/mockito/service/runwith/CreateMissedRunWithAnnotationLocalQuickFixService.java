package ru.migger.intellij.mockito.service.runwith;

import com.intellij.codeInspection.LocalQuickFix;
import com.intellij.psi.PsiClass;
import org.jetbrains.annotations.NotNull;

public interface CreateMissedRunWithAnnotationLocalQuickFixService {
    @NotNull
    LocalQuickFix[] createLocalQuickFixes(@NotNull PsiClass psiClass);
}
