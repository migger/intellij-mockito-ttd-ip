package ru.migger.intellij.mockito.service.runwith;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.psi.PsiField;
import org.jetbrains.annotations.NotNull;
import ru.migger.intellij.mockito.service.common.field.CheckFieldAnnotationService;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class CheckPsiFieldHaveMockitoAnnotationServiceImpl extends ApplicationComponent.Adapter implements CheckPsiFieldHaveMockitoAnnotationService {
    public static final Set<String> MOCKITO_ANNOTATIONS_QUALIFIED_NAMES = new HashSet<>(Arrays.asList(
            "org.mockito.InjectMocks",
            "org.mockito.Mock"
    ));
    private final CheckFieldAnnotationService checkFieldAnnotationService;

    public CheckPsiFieldHaveMockitoAnnotationServiceImpl(CheckFieldAnnotationService checkFieldAnnotationService) {
        this.checkFieldAnnotationService = checkFieldAnnotationService;
    }

    @Override
    public boolean isPsiFiledHaveMockitoAnnotation(@NotNull PsiField psiField) {
        for (String qualifiedName : MOCKITO_ANNOTATIONS_QUALIFIED_NAMES) {
            if (checkFieldAnnotationService.isFieldAnnotated(psiField, qualifiedName)) {
                return true;
            }
        }
        return false;
    }
}
