package ru.migger.intellij.mockito.service.runwith;

import com.intellij.codeInspection.LocalQuickFix;
import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.psi.PsiClass;
import org.jetbrains.annotations.NotNull;
import ru.migger.intellij.mockito.service.common.aclass.FindPsiClassService;
import ru.migger.intellij.mockito.service.common.modify.EnsureClassesIsImportedService;
import ru.migger.intellij.mockito.service.runwith.quickfix.MissedRunWithAnnotationLocalQuickFix;

public class CreateMissedRunWithAnnotationLocalQuickFixServiceImpl extends ApplicationComponent.Adapter implements CreateMissedRunWithAnnotationLocalQuickFixService {
    private final EnsureClassesIsImportedService ensureClassesIsImportedService;
    private final FindPsiClassService findPsiClassService;

    public CreateMissedRunWithAnnotationLocalQuickFixServiceImpl(
            EnsureClassesIsImportedService ensureClassesIsImportedService,
            FindPsiClassService findPsiClassService) {

        this.ensureClassesIsImportedService = ensureClassesIsImportedService;
        this.findPsiClassService = findPsiClassService;
    }

    @NotNull
    @Override
    public LocalQuickFix[] createLocalQuickFixes(@NotNull PsiClass psiClass) {
        final MissedRunWithAnnotationLocalQuickFix missedRunWithAnnotationLocalQuickFix = MissedRunWithAnnotationLocalQuickFix.create(findPsiClassService, ensureClassesIsImportedService, psiClass);
        return new LocalQuickFix[]{
                missedRunWithAnnotationLocalQuickFix
        };
    }
}
