package ru.migger.intellij.mockito.service.correspondingfield;

import com.intellij.codeInspection.LocalQuickFix;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;

public interface MissedCorrespondingRealFieldQuickFixCreateService {
    LocalQuickFix createLocalQuickFixes(PsiField psiField, PsiClass psiClass);
}
