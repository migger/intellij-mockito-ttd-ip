package ru.migger.intellij.mockito.service.correspondingfield;

import com.intellij.codeInspection.InspectionManager;
import com.intellij.codeInspection.LocalQuickFix;
import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.util.Pair;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiIdentifier;
import org.jetbrains.annotations.Nullable;
import ru.migger.intellij.mockito.service.common.field.FindPsiFieldWithoutAnnotationBoundsService;

public class MissedCorrespondingRealFieldProblemDescriptorCreateServiceImpl extends ApplicationComponent.Adapter implements MissedCorrespondingRealFieldProblemDescriptorCreateService {
    private final MissedCorrespondingRealFieldQuickFixCreateService missedCorrespondingRealFieldQuickFixCreateService;
    private final FindPsiFieldWithoutAnnotationBoundsService findPsiFieldWithoutAnnotationBoundsService;

    public MissedCorrespondingRealFieldProblemDescriptorCreateServiceImpl(MissedCorrespondingRealFieldQuickFixCreateService missedCorrespondingRealFieldQuickFixCreateService, FindPsiFieldWithoutAnnotationBoundsService findPsiFieldWithoutAnnotationBoundsService) {
        this.missedCorrespondingRealFieldQuickFixCreateService = missedCorrespondingRealFieldQuickFixCreateService;
        this.findPsiFieldWithoutAnnotationBoundsService = findPsiFieldWithoutAnnotationBoundsService;
    }

    @Nullable
    @Override
    public ProblemDescriptor createProblemDescriptors(PsiField psiField, PsiClass psiClass, InspectionManager inspectionManager) {
        final Pair<PsiElement, PsiElement> bounds = findPsiFieldWithoutAnnotationBoundsService.findPsiFieldWithoutAnnotationsBounds(psiField);
        final LocalQuickFix localQuickFixes = missedCorrespondingRealFieldQuickFixCreateService.createLocalQuickFixes(psiField, psiClass);
        if (localQuickFixes == null) {
            return inspectionManager.createProblemDescriptor(
                    bounds.getFirst(), bounds.getSecond(),
                    String.format("Class %s have no filed '%s %s'", psiClass.getName(), psiField.getType().getCanonicalText(), psiField.getName()),
                    ProblemHighlightType.GENERIC_ERROR, true);
        } else {
            return inspectionManager.createProblemDescriptor(
                    bounds.getFirst(), bounds.getSecond(),
                    String.format("Class %s have no filed '%s %s'", psiClass.getName(), psiField.getType().getCanonicalText(), psiField.getName()),
                    ProblemHighlightType.GENERIC_ERROR, true, localQuickFixes);
        }
    }
}
