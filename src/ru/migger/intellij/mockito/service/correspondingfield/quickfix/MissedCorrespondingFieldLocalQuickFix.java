package ru.migger.intellij.mockito.service.correspondingfield.quickfix;

import com.intellij.codeInspection.LocalQuickFix;
import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiJavaFile;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import ru.migger.intellij.mockito.service.common.aclass.FindPsiClassService;
import ru.migger.intellij.mockito.service.common.field.GetFieldClassTypeService;
import ru.migger.intellij.mockito.service.common.modify.AddFieldService;
import ru.migger.intellij.mockito.service.common.modify.EnsureClassesIsImportedService;

public class MissedCorrespondingFieldLocalQuickFix implements LocalQuickFix {
    private final PsiField psiField;
    private final PsiClass psiClass;
    private final EnsureClassesIsImportedService ensureClassesIsImportedService;
    private final FindPsiClassService findPsiClassService;
    private final GetFieldClassTypeService getFieldClassTypeService;
    private final AddFieldService addFieldService;

    public static MissedCorrespondingFieldLocalQuickFix create(PsiField psiField, PsiClass psiClass, EnsureClassesIsImportedService ensureClassesIsImportedService, FindPsiClassService findPsiClassService, GetFieldClassTypeService getFieldClassTypeService, AddFieldService addFieldService) {
        return new MissedCorrespondingFieldLocalQuickFix(psiField, psiClass, ensureClassesIsImportedService, findPsiClassService, getFieldClassTypeService, addFieldService);
    }

    private MissedCorrespondingFieldLocalQuickFix(PsiField psiField, PsiClass psiClass, EnsureClassesIsImportedService ensureClassesIsImportedService, FindPsiClassService findPsiClassService, GetFieldClassTypeService getFieldClassTypeService, AddFieldService addFieldService) {
        this.psiField = psiField;
        this.psiClass = psiClass;
        this.ensureClassesIsImportedService = ensureClassesIsImportedService;
        this.findPsiClassService = findPsiClassService;
        this.getFieldClassTypeService = getFieldClassTypeService;
        this.addFieldService = addFieldService;
    }
    @Nls
    @NotNull
    @Override
    public String getName() {
        return String.format("Add '%s %s' to class %s", psiField.getType().getCanonicalText(), psiField.getName(), psiClass.getName());
    }

    @NotNull
    @Override
    public String getFamilyName() {
        return "MockitoTTD";
    }

    @Override
    public void applyFix(@NotNull Project project, @NotNull ProblemDescriptor problemDescriptor) {
        final PsiClass autowiredClass = findPsiClassService.findClassByFullName(project, "org.springframework.beans.factory.annotation.Autowired");
        final PsiClass fieldType = getFieldClassTypeService.getClassType(project, psiField);
        final PsiField copy = (PsiField) psiField.copy();
        if (autowiredClass == null || fieldType == null || copy.getModifierList() == null)
            return;
        ensureClassesIsImportedService.ensureIsImported((PsiJavaFile) psiClass.getContainingFile(), autowiredClass);
        ensureClassesIsImportedService.ensureIsImported((PsiJavaFile) psiClass.getContainingFile(), fieldType);

        copy.getModifierList().setModifierProperty("private", true);
        copy.getModifierList().setModifierProperty("public", false);
        copy.getModifierList().setModifierProperty("protected", false);
        for (PsiAnnotation psiAnnotation : copy.getModifierList().getAnnotations()) {
            copy.getModifierList().deleteChildRange(psiAnnotation, psiAnnotation);
        }
        copy.getModifierList().addAnnotation("Autowired");
        addFieldService.addField(psiClass, copy);
    }
}
