package ru.migger.intellij.mockito.service.correspondingfield;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import org.jetbrains.annotations.NotNull;
import ru.migger.intellij.mockito.service.common.field.CheckFieldAnnotationService;

import java.util.ArrayList;
import java.util.List;

public class FindAllInjectMocksSiblingsServiceImpl extends ApplicationComponent.Adapter implements FindAllInjectMocksSiblingsService {
    private final CheckFieldAnnotationService checkFieldAnnotationService;

    public FindAllInjectMocksSiblingsServiceImpl(CheckFieldAnnotationService checkFieldAnnotationService) {
        this.checkFieldAnnotationService = checkFieldAnnotationService;
    }

    @NotNull
    @Override
    public PsiField[] findAllInjectMocksSiblings(@NotNull PsiField psiField) {
        final List<PsiField> ret = new ArrayList<>();
        final PsiClass containingClass = psiField.getContainingClass();
        if (containingClass == null)
            return PsiField.EMPTY_ARRAY;
        for (PsiField field : containingClass.getFields()) {
            if (field != psiField && checkFieldAnnotationService.isFieldAnnotated(field, "org.mockito.InjectMocks")) {
                ret.add(field);
            }
        }
        return ret.toArray(new PsiField[ret.size()]);
    }
}
