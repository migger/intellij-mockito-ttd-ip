package ru.migger.intellij.mockito.service.correspondingfield;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import org.jetbrains.annotations.NotNull;

public interface CheckMockFieldHaveCorrespondingRealFieldService {
    boolean isMockFieldHaveCorrespondingRealField(@NotNull PsiField psiField, @NotNull PsiClass psiClass);
}
