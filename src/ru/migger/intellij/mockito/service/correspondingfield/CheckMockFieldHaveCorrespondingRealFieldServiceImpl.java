package ru.migger.intellij.mockito.service.correspondingfield;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import org.jetbrains.annotations.NotNull;

public class CheckMockFieldHaveCorrespondingRealFieldServiceImpl extends ApplicationComponent.Adapter implements CheckMockFieldHaveCorrespondingRealFieldService {
    private final FieldMatchingService fieldMatchingService;

    public CheckMockFieldHaveCorrespondingRealFieldServiceImpl(FieldMatchingService fieldMatchingService) {

        this.fieldMatchingService = fieldMatchingService;
    }

    @Override
    public boolean isMockFieldHaveCorrespondingRealField(@NotNull PsiField psiField, @NotNull PsiClass psiClass) {
        for (PsiField field : psiClass.getFields()) {
            if (fieldMatchingService.isFieldsMatched(psiField, field)) {
                return true;
            }
        }
        return false;
    }
}
