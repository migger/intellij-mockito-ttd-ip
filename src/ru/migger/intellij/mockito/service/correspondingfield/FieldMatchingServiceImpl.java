package ru.migger.intellij.mockito.service.correspondingfield;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.psi.PsiField;

public class FieldMatchingServiceImpl extends ApplicationComponent.Adapter implements FieldMatchingService {

    @Override
    @SuppressWarnings("SimplifiableIfStatement")
    public boolean isFieldsMatched(PsiField field1, PsiField field2) {
        if (field1.getName() == null)
            return false;
        if (field2.getName() == null)
            return false;
        if (!field1.getName().equals(field2.getName())) {
            return false;
        }
        return field1.getType().equals(field2.getType());
    }
}
