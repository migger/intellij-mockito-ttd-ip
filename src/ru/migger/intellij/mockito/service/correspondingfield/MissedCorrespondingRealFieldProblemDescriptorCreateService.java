package ru.migger.intellij.mockito.service.correspondingfield;

import com.intellij.codeInspection.InspectionManager;
import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import org.jetbrains.annotations.Nullable;

public interface MissedCorrespondingRealFieldProblemDescriptorCreateService {
    @Nullable
    ProblemDescriptor createProblemDescriptors(PsiField psiField, PsiClass psiClass, InspectionManager inspectionManager);
}
