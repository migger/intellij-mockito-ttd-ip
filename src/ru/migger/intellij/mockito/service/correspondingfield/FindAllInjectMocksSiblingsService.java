package ru.migger.intellij.mockito.service.correspondingfield;

import com.intellij.psi.PsiField;
import org.jetbrains.annotations.NotNull;

public interface FindAllInjectMocksSiblingsService {
    @NotNull
    PsiField[] findAllInjectMocksSiblings(@NotNull PsiField psiField);
}
