package ru.migger.intellij.mockito.service.correspondingfield;

import com.intellij.codeInspection.LocalQuickFix;
import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import ru.migger.intellij.mockito.service.common.aclass.FindPsiClassService;
import ru.migger.intellij.mockito.service.common.field.GetFieldClassTypeService;
import ru.migger.intellij.mockito.service.common.modify.AddFieldService;
import ru.migger.intellij.mockito.service.common.modify.EnsureClassesIsImportedService;
import ru.migger.intellij.mockito.service.correspondingfield.quickfix.MissedCorrespondingFieldLocalQuickFix;

public class MissedCorrespondingRealFieldQuickFixCreateServiceImpl extends ApplicationComponent.Adapter implements MissedCorrespondingRealFieldQuickFixCreateService {
    private final EnsureClassesIsImportedService ensureClassesIsImportedService;
    private final FindPsiClassService findPsiClassService;
    private final GetFieldClassTypeService getFieldClassTypeService;
    private final AddFieldService addFieldService;

    public MissedCorrespondingRealFieldQuickFixCreateServiceImpl(EnsureClassesIsImportedService ensureClassesIsImportedService, FindPsiClassService findPsiClassService, GetFieldClassTypeService getFieldClassTypeService, AddFieldService addFieldService) {
        this.ensureClassesIsImportedService = ensureClassesIsImportedService;
        this.findPsiClassService = findPsiClassService;
        this.getFieldClassTypeService = getFieldClassTypeService;
        this.addFieldService = addFieldService;
    }

    @Override
    public LocalQuickFix createLocalQuickFixes(PsiField psiField, PsiClass psiClass) {
        if (findPsiClassService.findClassByFullName(psiClass.getProject(), "org.springframework.beans.factory.annotation.Autowired") == null) {
            return null;
        }
        return MissedCorrespondingFieldLocalQuickFix.create(psiField, psiClass, ensureClassesIsImportedService, findPsiClassService, getFieldClassTypeService, addFieldService);
    }
}
