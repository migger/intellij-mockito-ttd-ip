package ru.migger.intellij.mockito.service.correspondingfield;

import com.intellij.psi.PsiField;

public interface FieldMatchingService {
    boolean isFieldsMatched(PsiField field1, PsiField field2);
}
