package ru.migger.intellij.mockito;

import com.intellij.codeInspection.BaseJavaLocalInspectionTool;
import com.intellij.codeInspection.InspectionManager;
import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiField;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.migger.intellij.mockito.service.common.field.CheckFieldAnnotationService;
import ru.migger.intellij.mockito.service.common.field.FindPsiFieldInitializerClassService;
import ru.migger.intellij.mockito.service.correspondingfield.CheckMockFieldHaveCorrespondingRealFieldService;
import ru.migger.intellij.mockito.service.correspondingfield.FindAllInjectMocksSiblingsService;
import ru.migger.intellij.mockito.service.correspondingfield.MissedCorrespondingRealFieldProblemDescriptorCreateService;

import java.util.ArrayList;
import java.util.List;

public class MockitoMockCorrespondingFieldLocalInspection extends BaseJavaLocalInspectionTool {
    private static final Logger LOG = Logger.getInstance("#rru.migger.intellij.mockito.MockitoMockCorrespondingFieldLocalInspection");
    private final CheckFieldAnnotationService checkFieldAnnotationService;
    private final CheckMockFieldHaveCorrespondingRealFieldService checkMockFieldHaveCorrespondingRealFieldService;
    private final FindAllInjectMocksSiblingsService findAllInjectMocksSiblingsService;
    private final FindPsiFieldInitializerClassService findPsiFieldInitializerClassService;
    private final MissedCorrespondingRealFieldProblemDescriptorCreateService missedCorrespondingRealFieldProblemDescriptorCreateService;

    public MockitoMockCorrespondingFieldLocalInspection(CheckFieldAnnotationService checkFieldAnnotationService, CheckMockFieldHaveCorrespondingRealFieldService checkMockFieldHaveCorrespondingRealFieldService, FindAllInjectMocksSiblingsService findAllInjectMocksSiblingsService, FindPsiFieldInitializerClassService findPsiFieldInitializerClassService, MissedCorrespondingRealFieldProblemDescriptorCreateService missedCorrespondingRealFieldProblemDescriptorCreateService) {
        this.checkFieldAnnotationService = checkFieldAnnotationService;
        this.checkMockFieldHaveCorrespondingRealFieldService = checkMockFieldHaveCorrespondingRealFieldService;
        this.findAllInjectMocksSiblingsService = findAllInjectMocksSiblingsService;
        this.findPsiFieldInitializerClassService = findPsiFieldInitializerClassService;
        this.missedCorrespondingRealFieldProblemDescriptorCreateService = missedCorrespondingRealFieldProblemDescriptorCreateService;
    }

    @Nullable
    @Override
    public ProblemDescriptor[] checkField(@NotNull PsiField field, @NotNull InspectionManager manager, boolean isOnTheFly) {
        final List<ProblemDescriptor> allProblem = new ArrayList<>();
        if (!checkFieldAnnotationService.isFieldAnnotated(field, "org.mockito.Mock"))
            return null;
        for (PsiField psiField : findAllInjectMocksSiblingsService.findAllInjectMocksSiblings(field)) {
            final PsiClass psiClass = findPsiFieldInitializerClassService.findInitializerClass(psiField);
            if (psiClass == null || checkMockFieldHaveCorrespondingRealFieldService.isMockFieldHaveCorrespondingRealField(field, psiClass)) {
                continue;
            }
            final ProblemDescriptor problemDescriptors = missedCorrespondingRealFieldProblemDescriptorCreateService.createProblemDescriptors(field, psiClass, manager);
            if (problemDescriptors != null) {
                allProblem.add(problemDescriptors);
            }
        }
        if (allProblem.isEmpty())
            return null;
        return allProblem.toArray(new ProblemDescriptor[allProblem.size()]);
    }
}
